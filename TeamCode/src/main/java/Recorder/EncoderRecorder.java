package Recorder;

import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class EncoderRecorder {

    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;


    boolean StopRecord;
    Telemetry telemetry;



    public EncoderRecorder(DcMotor FL, DcMotor FR, DcMotor RL, DcMotor RR, DcMotor winch, Telemetry telemetry) {


        this.FL =FL;
        this.FR = FR;
        this.RL = RL;
        this.RR = RR;
        this.arm = winch;
        this.telemetry = telemetry;
        reset();
    }




    public void run(int FL, int FR, int RL, int RR,int arm, double power, int accuracy) {
        reset();
        while (!isOnTarget( FL, FR, RL, RR, arm)&& isOpModeActive()) {
            ;
            this.FL.setTargetPosition(FL);
            this.FR.setTargetPosition(FR);
            this.RL.setTargetPosition(RL);
            this.RR.setTargetPosition(RR);
            this.arm.setTargetPosition(arm);


            this.FL.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.FR.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.RL.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.RR.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            this.arm.setMode(DcMotor.RunMode.RUN_TO_POSITION);


            this.FL.setPower(power);
            this.FR.setPower(power);
            this.RL.setPower(power);
            this.RR.setPower(power);
            this.arm.setPower(power);


            output(this.FL.getCurrentPosition(), this.FR.getCurrentPosition(), this.RL.getCurrentPosition(), this.RR.getCurrentPosition(), this.arm.getCurrentPosition());
        }
    }

    private boolean isOpModeActive() {
        return true;
    }

    boolean recordFirstRun = false;
    boolean stopRecord;
    //drive robot and record encoder pos
    public void record(double FL, double FR, double RL, double RR, double arm, boolean stop) throws InterruptedException {
        int[] data = new int[5];
        if(!recordFirstRun) {
            recordFirstRun = true;
            stopRecord = false;
            reset();
        }
        if(!stopRecord) {
            data[0] = runMotor(this.FL, FL);
            data[1] = runMotor(this.FR, FR);
            data[2] = runMotor(this.RL, RL);
            data[3] = runMotor(this.RR, RR);
            data[4] = runMotor(this.arm, arm);


            if(stop) stopRecord = true;
        } else {
            recordFirstRun = false;
            while(true) {
                data[0] = runMotor(this.FL, 0);
                data[1] = runMotor(this.FR, 0);
                data[2] = runMotor(this.RL, 0);
                data[3] = runMotor(this.RR, 0);
                data[4] = runMotor(this.arm, 0);


                Thread.sleep(50);
            }
        }
        output(data[0], data[1], data[2], data[3], data[4]);
    }

    //run a single motor and return encoder pos
    private int runMotor(DcMotor motor, double power) {
        motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor.setPower(power);
        return motor.getCurrentPosition();
    }

    //reset all encoder pos
    public void reset() {
        FL.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        FR.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        RL.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        RR.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        arm.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


    }

    private void output(int FL, int FR, int RL, int RR, int arm) { //i hate the telemetry class
        telemetry.addData("FL pos", FL);
        telemetry.addData("FR pos", FR);
        telemetry.addData("RL pos", RL);
        telemetry.addData("RR pos", RR);
        telemetry.addData("arm pos", arm);


        telemetry.update();
    }

    private boolean isOnTarget(int targ1, int targ2, int targ3, int targ4, int targ5) {
        int success = 0;
        int accuracy = 0;
        if (Math.abs(this.FL.getCurrentPosition() - targ1) <= accuracy) success++;
        if (Math.abs(this.FR.getCurrentPosition() - targ2) <= accuracy) success++;
        if (Math.abs(this.RL.getCurrentPosition() - targ3) <= accuracy) success++;
        if (Math.abs(this.RR.getCurrentPosition() - targ4) <= accuracy) success++;
        if (Math.abs(this.arm.getCurrentPosition() - targ5) <= accuracy) success++;


        return success >= 2;
    }
}
