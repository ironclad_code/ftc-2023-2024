package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import Recorder.EncoderRecorder;

@Autonomous(name="BL1", group="Robot")
//@Disabled
public class BL1 extends LinearOpMode {

    /* Declare OpMode members. */
    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;
    Servo wrist;
    Servo claw;
    EncoderRecorder recorder;

    //@Overide
    public void runOpMode() {

        // Initialize the drive system variables.
        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        wrist = hardwareMap.get(Servo.class, "wrist");
        claw = hardwareMap.get(Servo.class, "claw");


        recorder = new EncoderRecorder(FL,FR,RL,RR,arm, telemetry);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Status", "Ready to run");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();{

        while (opModeIsActive()) { // Add a loop to continually update telemetry

           // place a preloaded pixel on middle spike
            /* claw.setPosition(1); // close claw
            sleep(2000);
            recorder.run(-997, 997, -993, 983,1104, 0.5, 1); //forward + arm down
            sleep(2000);
            wrist.setPosition(-1); // wrist down
            sleep(2000);
            claw.setPosition(-1);//open claw*/

            //place preloaded pixel on right spike
            /*claw.setPosition(1); // close claw
            sleep(2000);
            recorder.run(-818, 818, -810, 818,1298, 0.5, 1); //forward + arm down
            recorder.run(-719, -706, 710, 669,0, 0.5, 1); //strafe right
            sleep(2000);
            wrist.setPosition(-1); // wrist down
            sleep(2000);
            claw.setPosition(-1); //open claw*/

            //place preloaded pixel on left spike
            //claw.setPosition(1); // close claw
            //sleep(2000);
            //recorder.run(-1284, 1275, -1265, 1272,1298, 0.5, 1); //forward + arm down
            //recorder.run(976, 980, 958, 942  ,0, 0.5, 1); //turn left
            //recorder.run(37, -41, 37, -41  ,0, 0.5, 1); //back
            //sleep(2000);
            //wrist.setPosition(-1); // wrist down
            //sleep(2000);
            //claw.setPosition(-1); //open claw





            /* recorder.run(-1196, 1189, -1185, 1174,0, 0.5, 1); //forward
            recorder.run(-788, -756, -724, -733,0, 0.5, 1); //turn right
            recorder.run(-1395, 1392, -1388, 1361, 0,0.5, 1); //forward
            sleep(1000); //pause
            //extra turns instead of strafe
            recorder.run(-832, -808, -771, -778,0,  0.5, 1);//turn right
            recorder.run(-802, 794, -791, 781,0, 0.5, 1); //forward
            recorder.run(737, 714, 677, 680, 0,0.5, 1); //turn left
            recorder.run(-673, 665, -663, 655,0, 0.5, 1); //forward */
            //strafe option
            // recorder.run(-1416, -1387, 1384, 1358, 0.5, 1); //strafe right
            // recorder.run(-800, 782, -783, 777, 0.5, 1); //forward
            //strafe test
            /* recorder.run(-959, -938, 933, 904, 0.5, 1); // strafe right 1
            recorder.run(988, 952, -971, -932, 0.5, 1); // strafe left 1
            recorder.run(-1117, -1095, 1084, 1070, 0.5, 1); // strafe right 2
            recorder.run(1269, 1236, -1251, -1215, 0.5, 1); // strafe left 2 */

            //place pixle on back drop BLUE LEFT
            claw.setPosition(1); // close claw
            recorder.run(-200, 194, -197, 190,0, 0.5, 5);//forward
            recorder.run(981, 983, 961, 940,-1000, 0.5, 5);//turn left
            recorder.run(-1187, 1190, -1185, 1170,-1000, 0.5, 5);//forward
            recorder.run(-1007, -991, -961, -977,-1000, 0.5, 5);//turn right
            recorder.run(-883, 882, -877, 877,-1000, 0.5, 5);//forward
            recorder.run(-943, -926, -891, -902,-550, 0.5, 5);//turn right and arm up
            recorder.run(637, -641, 635, -625,-500, 0.5, 5);//back
            sleep(1000);
            claw.setPosition(-1);//open claw
            sleep(900);
            recorder.run(-1047, -1058, 1019, 999,1000, 0.5, 5); //strafe right after dropping pixel
            sleep(20000000);






            telemetry.update();
            stop();

        }
        }
    }
}
