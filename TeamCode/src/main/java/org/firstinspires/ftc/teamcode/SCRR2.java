


package org.firstinspires.ftc.teamcode;


import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import Recorder.EncoderRecorder;


@Autonomous(name = "SCRR2", group = "Sensor")


public class SCRR2 extends LinearOpMode {


    /** The colorSensor field will contain a reference to our color sensor hardware object */
    NormalizedColorSensor CS;
    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;
    Servo wrist;
    Servo claw;
    Servo dropper;
    EncoderRecorder recorder;

    View relativeLayout;

    int counter = 0;

    //public void updateColorSensor(){

    //}

    @Override public void runOpMode() {

        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        wrist = hardwareMap.get(Servo.class, "wrist");
        claw = hardwareMap.get(Servo.class, "claw");
        dropper = hardwareMap.get(Servo.class, "dropper");
        recorder = new EncoderRecorder(FL,FR,RL,RR,arm, telemetry);

        // Get a reference to the RelativeLayout so we can later change the background
        // color of the Robot Controller app to match the hue detected by the RGB sensor.
        int relativeLayoutId = hardwareMap.appContext.getResources().getIdentifier("RelativeLayout", "id", hardwareMap.appContext.getPackageName());
        relativeLayout = ((Activity) hardwareMap.appContext).findViewById(relativeLayoutId);


        try {
            runSample(); // actually execute the sample
        } finally {
            // On the way out, *guarantee* that the background is reasonable. It doesn't actually start off
            // as pure white, but it's too much work to dig out what actually was used, and this is good
            // enough to at least make the screen reasonable again.
            // Set the panel back to the default color
            relativeLayout.post(new Runnable() {
                public void run() {
                    relativeLayout.setBackgroundColor(Color.WHITE);
                }
            });
        }
    }


    protected void runSample() {

        float gain = 2;


        // Once per loop, we will update this hsvValues array. The first element (0) will contain the
        // hue, the second element (1) will contain the saturation, and the third element (2) will
        // contain the value. See http://web.archive.org/web/20190311170843/https://infohost.nmt.edu/tcc/help/pubs/colortheory/web/hsv.html
        // for an explanation of HSV color.
        final float[] hsvValues = new float[3];


        // xButtonPreviouslyPressed and xButtonCurrentlyPressed keep track of the previous and current
        // state of the X button on the gamepad
        boolean xButtonPreviouslyPressed = false;
        boolean xButtonCurrentlyPressed = false;


        // Get a reference to our sensor object. It's recommended to use NormalizedColorSensor over
        // ColorSensor, because NormalizedColorSensor consistently gives values between 0 and 1, while
        // the values you get from ColorSensor are dependent on the specific sensor you're using.
        CS = hardwareMap.get(NormalizedColorSensor.class, "CS");


        // If possible, turn the light on in the beginning (it might already be on anyway,
        // we just make sure it is if we can).
        if (CS instanceof SwitchableLight) {
            ((SwitchableLight)CS).enableLight(true);
        }


        // Wait for the start button to be pressed.
        waitForStart();


        // Loop until we are asked to stop
        while (opModeIsActive()) {

            // Explain basic gain information via telemetry
            telemetry.addLine("Hold the A button on gamepad 1 to increase gain, or B to decrease it.\n");
            telemetry.addLine("Higher gain values mean that the sensor will report larger numbers for Red, Green, and Blue, and Value\n");


            // Update the gain value if either of the A or B gamepad buttons is being held
            if (gamepad1.a) {
                // Only increase the gain by a small amount, since this loop will occur multiple times per second.
                gain += 0.005;
            } else if (gamepad1.b && gain > 1) { // A gain of less than 1 will make the values smaller, which is not helpful.
                gain -= 0.005;
            }


            // Show the gain value via telemetry
            telemetry.addData("Gain", gain);


            // Tell the sensor our desired gain value (normally you would do this during initialization,
            // not during the loop)
            CS.setGain(gain);


            // Check the status of the X button on the gamepad
            xButtonCurrentlyPressed = gamepad1.x;


            // If the button state is different than what it was, then act
            if (xButtonCurrentlyPressed != xButtonPreviouslyPressed) {
                // If the button is (now) down, then toggle the light
                if (xButtonCurrentlyPressed) {
                    if (CS instanceof SwitchableLight) {
                        SwitchableLight light = (SwitchableLight) CS;
                        light.enableLight(!light.isLightOn());
                    }
                }
            }
            xButtonPreviouslyPressed = xButtonCurrentlyPressed;


            // Get the normalized colors from the sensor
            NormalizedRGBA colors = CS.getNormalizedColors();


            // Update the hsvValues array by passing it to Color.colorToHSV()
            Color.colorToHSV(colors.toColor(), hsvValues);

            telemetry.addLine()
                    .addData("Red", "%.3f", colors.red)
                    .addData("Green", "%.3f", colors.green)
                    .addData("Blue", "%.3f", colors.blue);
            telemetry.addLine()
                    .addData("Hue", "%.3f", hsvValues[0])
                    .addData("Saturation", "%.3f", hsvValues[1])
                    .addData("Value", "%.3f", hsvValues[2]);
            telemetry.addData("Alpha", "%.3f", colors.alpha);


            if (CS instanceof DistanceSensor) {
                telemetry.addData("Distance (cm)", "%.3f", ((DistanceSensor) CS).getDistance(DistanceUnit.CM));
            }


            telemetry.update();


            // Change the Robot Controller's background color to match the color detected by the color sensor.
            relativeLayout.post(new Runnable() {
                public void run() {
                    relativeLayout.setBackgroundColor(Color.HSVToColor(hsvValues));
                }
            });


            counter++;
            //if counter == 1 go to right spike
            //if counter == 2 & sees color else if counter == 2 check middle
            //if counter == 3 & color else left spike

            if (counter==1) {
                //check right spike
                claw.setPosition(1);
                sleep(500);
                recorder.run(-950, 950, -950, 950, 0, 0.5, 5);//forward
                recorder.run(-87, -93, 76, 65, 0, 0.5, 5);//strafe right
                recorder.run(-70, 70, -70, 70, 0, 0.5, 5);//forward
            }

            if(counter==2&&hsvValues[0]!=0) {
                //sees right spike
                recorder.run(-500, -500, 500, 500, 0, 0.5, 5);//strafe right
                dropper.setPosition(1);
                sleep(500);
                dropper.setPosition(0);
                sleep(500);
                dropper.setPosition(1);
                sleep(500);
                recorder.run(300, -300, 300, -300, 0, 0.5, 5);//backward
                recorder.run(-461, -465, 471, 439, 0, 0.5, 5);//strafe right
                recorder.run(969, 977, 967, 942, -1000, 0.5, 5);//turn left
                recorder.run(-3865, -3865, -3865, -3865,-3800, 0.5, 5);//spin right
                recorder.run(-450, -450, 450, 450, 0, 0.5, 5);//strafe right
                recorder.run(800, -800, 800, -800, -700, 0.5, 5);//backwards
                claw.setPosition(0);//open claw
                sleep(1000);
                recorder.run(500, 500, -500, -500, 500, 0.5, 5);//strafe left
                sleep(10000);
            }
            else if(counter==2) {
                //move to check middle spike
                recorder.run(-300, -300, 300, 300, 0, 0.5, 5);//strafe right
                recorder.run(964, 971, 939, 921, 0, 0.5, 5);//turn left
                recorder.run(-578, -565, 572, 541, 0, 0.5, 5);//strafe right
                recorder.run(-100, 100, -100, 100, 0, 0.5, 5);//forward
            }
            if(counter==3&&hsvValues[0]!=0) {
                //sees middle
                recorder.run(-400, -400, 400, 400, -200, 0.5, 5);//strafe right
                dropper.setPosition(1);
                sleep(500);
                dropper.setPosition(0);
                sleep(500);
                dropper.setPosition(1);
                sleep(500);
                recorder.run(700, -700, 700, -700, -700, 0.5, 5);//backwards
                recorder.run(-3800, -3800, -3800, -3800,-3800, 0.5, 5);//spin right
                recorder.run(635, 641, -650, -620, -300, 0.5, 5);//strafe left
                recorder.run(700, -700, 700, -700, -650, 0.5, 5);//backwards
                claw.setPosition(0);//open claw
                sleep(1000);
                recorder.run(1000, 1000, -1000, -1000, 500, 0.5, 5);//strafe left
                sleep(10000);
            }
            else if (counter==3)
            {
                //do left spike
                recorder.run(250, 250, -250, -250, 0, 0.5, 5);//strafe left
                recorder.run(-667, 669, -675, 657, -300, 0.5, 5);//forward
                dropper.setPosition(1);
                sleep(500);
                dropper.setPosition(0);
                sleep(500);
                dropper.setPosition(1);
                sleep(500);
                recorder.run(1000, -1000, 1000, -1000, -1000, 0.5, 5);//backward
                recorder.run(-3800, -3800, -3800, -3800,-3800, 0.5, 5);//spin right
                recorder.run(-150, -150, 150, 150, -100, 0.5, 5);//strafe right
                recorder.run(1000, -1000, 1000, -1000, -1000, 0.5, 5);//backward
                claw.setPosition(0);//open claw
                sleep(1000);
                recorder.run(1500, 1500, -1500, -1500, 1000, 0.5, 5);//strafe left
                sleep(10000);
            }



            telemetry.update();

        }

    }

}

