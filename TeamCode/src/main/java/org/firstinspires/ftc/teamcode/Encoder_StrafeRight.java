package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import Recorder.EncoderRecorder;


@TeleOp(name="Encoder_StrafeRight", group="Encoder")
public class Encoder_StrafeRight extends LinearOpMode {

    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;
    EncoderRecorder recorder;

    @Override
    public void runOpMode() throws InterruptedException {
        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");

        recorder = new EncoderRecorder(FL,FR,RL,RR,arm, telemetry);
        waitForStart();
        if (opModeIsActive()) {
              while(true){
                        exampleRecordingCode:recorder.record(-0.1, -0.1, 0.1, 0.1,0, gamepad1.a);
                      }
            //recorder.run(5000, -5000, 5000, -5000, 1, 1);
           //recorder.run(5000, 5000, -5000, 5000, 1, 1);

        }
    }
}
