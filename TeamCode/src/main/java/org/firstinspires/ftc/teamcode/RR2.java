package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import Recorder.EncoderRecorder;

@Autonomous(name="RR2", group="Robot")
//@Disabled
public class RR2 extends LinearOpMode {

    /* Declare OpMode members. */
    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;
    Servo wrist;
    Servo claw;
    EncoderRecorder recorder;

    //@Overide
    public void runOpMode() {

        // Initialize the drive system variables.
        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        wrist = hardwareMap.get(Servo.class, "wrist");
        claw = hardwareMap.get(Servo.class, "claw");


        recorder = new EncoderRecorder(FL,FR,RL,RR,arm, telemetry);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Status", "Ready to run");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();{

        while (opModeIsActive()) { // Add a loop to continually update telemetry

            //place pixel on back drop BLUE RIGHT
            claw.setPosition(1); // close claw
            sleep(500);
            recorder.run(-886, 884, -876, 876,-1000, 0.5, 5);//forward + arm up
            recorder.run(-3890, -3865, -3836, -3851,-3500, 0.5, 5);//spin right
            recorder.run(938, 934, 911, 898,-1000, 0.5, 5);//turn left + arm up
            recorder.run(-523, -505, 502, 483,-500, 0.5, 5);//strafe right + arm up
            recorder.run(1600, -1600, 1600, -1600,-1500, 0.5, 5);//backward + arm up
            sleep(500);
            claw.setPosition(0); // open claw
            recorder.run(-1355, -1359, 1321, 1301,500, 0.5, 5);//strafe right
            sleep(1000000000);
            telemetry.update();
            stop();

        }
        }
    }
}
