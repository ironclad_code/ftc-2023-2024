package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import Recorder.EncoderRecorder;

@Autonomous(name="BR1", group="Robot")
//@Disabled
public class BR1 extends LinearOpMode {

    /* Declare OpMode members. */
    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;
    Servo wrist;
    Servo claw;
    EncoderRecorder recorder;



    //@Overide
    public void runOpMode() {

        // Initialize the drive system variables.
        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        wrist = hardwareMap.get(Servo.class, "wrist");
        claw = hardwareMap.get(Servo.class, "claw");


        recorder = new EncoderRecorder(FL,FR,RL,RR,arm, telemetry);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Status", "Ready to run");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();{

        while (opModeIsActive()) { // Add a loop to continually update telemetry

            //place pixel on back drop BLUE RIGHT
            claw.setPosition(1); // close claw
            sleep(500);
            recorder.run(-156, 154, -151, 156,100, 0.5, 5);//forward + arm down
            recorder.run(-991, -982, -966, -944,900, 0.5, 10);//turn right + arm down
            recorder.run(2300, -2300, 2300, -2300,0, 0.5, 5);//backward
            recorder.run(1000, -1000, 1000, -1000,-1000, 0.5, 5);//backward + arm up
            recorder.run(1100, 1100, -1100, -1100,-1000, 0.5, 5);//strafe left + arm up
            recorder.run(-3890, -3865, -3836, -3851,-3800, 0.5, 5);//spin right+ arm up
            recorder.run(850, -850, 850, -850,-1000, 0.5, 5);//backward + arm up
            sleep(500);
            claw.setPosition(0); // open claw
            sleep(1000);
            recorder.run(-1052, -1052, 1032, 1009,1000, 0.5, 5);//strafe right
            sleep(1000000000);
            telemetry.update();
            stop();

        }
        }
    }
}
