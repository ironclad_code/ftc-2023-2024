/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;



import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;


/*
 * This OpMode executes a Tank Drive control TeleOp a direct drive robot
 * The code is structured as an Iterative OpMode
 *
 * In this mode, the left and right joysticks control the left and right motors respectively.
 * Pushing a joystick forward will make the attached motor drive forward.
 * It raises and lowers the claw using the Gamepad Y and A buttons respectively.
 * It also opens and closes the claws slowly using the left and right Bumper buttons.
 *
 * Use Android Studio to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this OpMode to the Driver Station OpMode list
 */

@TeleOp(name="Robot: Driver Control bam", group="Robot")
//@Disabled
public class DriverControlled extends OpMode {

    /* Declare OpMode members. */
    public DcMotor FL = null;
    public DcMotor FR = null;
    public DcMotor RL = null;
    public DcMotor RR = null;
    private DcMotor arm = null;
    private Servo wrist = null;
    private Servo claw;
    private Servo DL;


    /*
     * Code to run ONCE when the driver hits INIT
     */
    // @Override
    public void init() {
        // Define and Initialize Motors
        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        wrist = hardwareMap.get(Servo.class, "wrist");
        claw = hardwareMap.get(Servo.class, "claw");
        DL = hardwareMap.get(Servo.class, "DL");

        // To drive forward, most robots need the motor on one side to be reversed, because the axles point in opposite directions.
        // Pushing the left and right sticks forward MUST make robot go forward. So adjust these two lines based on your first test drive.
        // Note: The settings here assume direct drive on left and right wheels.  Gear Reduction or 90 Deg drives may require direction flips
        FL.setDirection(DcMotor.Direction.REVERSE);
        FR.setDirection(DcMotor.Direction.FORWARD);
        RL.setDirection(DcMotor.Direction.REVERSE);
        RR.setDirection(DcMotor.Direction.FORWARD);


        FL.setZeroPowerBehavior(FL.getZeroPowerBehavior());
        FR.setZeroPowerBehavior(FR.getZeroPowerBehavior());
        RL.setZeroPowerBehavior(RL.getZeroPowerBehavior());
        RR.setZeroPowerBehavior(RR.getZeroPowerBehavior());

        double servoPosition = 0.5;

    }

    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    //@Override
    public void initloop() {
    }


    @Override
    public void loop() {

        double leftX; // double is numbers with decimal
        double leftY;
        double rightX;
        double leftX2;
        double leftY2;
        double rightY2;
        boolean a; // boolean is true or false
        boolean b;
        boolean x;
        boolean y;
        boolean left_bumper;
        boolean right_bumper;

        // Run wheels in tank mode (note: The joystick goes negative when pushed forward, so negate it)
        leftY = gamepad1.left_stick_y; // left joystick right to left controller 1
        leftX = gamepad1.left_stick_x; // left joystick up to down controller 1
        rightX = gamepad1.right_stick_x; // right joystick right to left controller 1
        leftY2 = gamepad2.left_stick_y; // right joystick up to down controller 2
        a = gamepad2.a; // A button on controller 2
        b = gamepad2.b; // B button on controller 2
        x = gamepad2.x; // X button on controller 2
        y = gamepad2.y; // Y button on controller 2

        left_bumper = gamepad2.left_bumper; // LB button on controller 2
        right_bumper = gamepad2.right_bumper; // LB button on controller 2
        //up-down
        FL.setPower(-leftY);
        FR.setPower(-leftY);
        RL.setPower(-leftY);
        RR.setPower(-leftY);
        //left-right
        FL.setPower(rightX);
        FR.setPower(-rightX);
        RL.setPower(rightX);
        RR.setPower(-rightX);
        //strafe
        FL.setPower(leftX);
        FR.setPower(-leftX);
        RL.setPower(-leftX);
        RR.setPower(leftX);

        //move arm
        arm.setPower(leftY2);

        // launch drone
        if (left_bumper) {
            DL.setPosition(-1);
        }

       if (right_bumper) {
           if (b) {
               claw.setPosition(-1);
               wrist.setPosition(1);
           }
       }
        //move wrist
        if (x) {
            wrist.setPosition(1);
        }
        if (y) {
            wrist.setPosition(0);
        }
        //open+close claw
            if (a) {
                claw.setPosition(-1);
            }
            if (b) {
                claw.setPosition(1);
            }


        }


        public void stop () {
        }
    }



