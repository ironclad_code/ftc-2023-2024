


package org.firstinspires.ftc.teamcode;


import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

import Recorder.EncoderRecorder;


@Autonomous(name = "SCRR1", group = "Sensor")


public class SCRR1 extends LinearOpMode {


    /** The colorSensor field will contain a reference to our color sensor hardware object */
    NormalizedColorSensor CS;
    DcMotor FL;
    DcMotor FR;
    DcMotor RL;
    DcMotor RR;
    DcMotor arm;
    Servo wrist;
    Servo claw;
    EncoderRecorder recorder;


    View relativeLayout;

    @Override public void runOpMode() {

        FL = hardwareMap.get(DcMotor.class, "FL");
        FR = hardwareMap.get(DcMotor.class, "FR");
        RL = hardwareMap.get(DcMotor.class, "RL");
        RR = hardwareMap.get(DcMotor.class, "RR");
        arm = hardwareMap.get(DcMotor.class, "arm");
        wrist = hardwareMap.get(Servo.class, "wrist");
        claw = hardwareMap.get(Servo.class, "claw");


        recorder = new EncoderRecorder(FL,FR,RL,RR,arm, telemetry);

        // Send telemetry message to signify robot waiting;
        telemetry.addData("Status", "Ready to run");
        telemetry.update();

        // Get a reference to the RelativeLayout so we can later change the background
        // color of the Robot Controller app to match the hue detected by the RGB sensor.
        int relativeLayoutId = hardwareMap.appContext.getResources().getIdentifier("RelativeLayout", "id", hardwareMap.appContext.getPackageName());
        relativeLayout = ((Activity) hardwareMap.appContext).findViewById(relativeLayoutId);


        try {
            runSample(); // actually execute the sample
        } finally {
            // On the way out, *guarantee* that the background is reasonable. It doesn't actually start off
            // as pure white, but it's too much work to dig out what actually was used, and this is good
            // enough to at least make the screen reasonable again.
            // Set the panel back to the default color
            relativeLayout.post(new Runnable() {
                public void run() {
                    relativeLayout.setBackgroundColor(Color.WHITE);
                }
            });
        }
    }


    protected void runSample() {

        //float gain = 2;

        final float[] hsvValues = new float[3];


        //boolean xButtonPreviouslyPressed = false;
        //boolean xButtonCurrentlyPressed = false;



        if (CS instanceof SwitchableLight) {
            ((SwitchableLight)CS).enableLight(true);
        }


        // Wait for the start button to be pressed.
        waitForStart();


        // Loop until we are asked to stop
        while (opModeIsActive()) {
            // Explain basic gain information via telemetry
           /* telemetry.addLine("Hold the A button on gamepad 1 to increase gain, or B to decrease it.\n");
            telemetry.addLine("Higher gain values mean that the sensor will report larger numbers for Red, Green, and Blue, and Value\n");


            // Update the gain value if either of the A or B gamepad buttons is being held
            if (gamepad1.a) {
                // Only increase the gain by a small amount, since this loop will occur multiple times per second.
                gain += 0.005;
            } else if (gamepad1.b && gain > 1) { // A gain of less than 1 will make the values smaller, which is not helpful.
                gain -= 0.005;
            }
*/

            // Show the gain value via telemetry
           // telemetry.addData("Gain", gain);


            // Tell the sensor our desired gain value (normally you would do this during initialization,
            // not during the loop)
            //CS.setGain(gain);


            // Check the status of the X button on the gamepad
           // xButtonCurrentlyPressed = gamepad1.x;


            /* If the button state is different than what it was, then act
            if (xButtonCurrentlyPressed != xButtonPreviouslyPressed) {
                // If the button is (now) down, then toggle the light
                if (xButtonCurrentlyPressed) {
                    if (CS instanceof SwitchableLight) {
                        SwitchableLight light = (SwitchableLight)CS;
                        light.enableLight(!light.isLightOn());
                    }
                }
            }
           xButtonPreviouslyPressed = xButtonCurrentlyPressed;*/


            // Get the normalized colors from the sensor
            NormalizedRGBA colors = CS.getNormalizedColors();


            /* Use telemetry to display feedback on the driver station. We show the red, green, and blue
             * normalized values from the sensor (in the range of 0 to 1), as well as the equivalent
             * HSV (hue, saturation and value) values. See http://web.archive.org/web/20190311170843/https://infohost.nmt.edu/tcc/help/pubs/colortheory/web/hsv.html
             * for an explanation of HSV color. */


            // Update the hsvValues array by passing it to Color.colorToHSV()
            Color.colorToHSV(colors.toColor(), hsvValues);


            telemetry.addLine()
                    .addData("Red", "%.3f", colors.red)
                    .addData("Green", "%.3f", colors.green)
                    .addData("Blue", "%.3f", colors.blue);
            telemetry.addLine()
                    .addData("Hue", "%.3f", hsvValues[0])
                    .addData("Saturation", "%.3f", hsvValues[1])
                    .addData("Value", "%.3f", hsvValues[2]);
            telemetry.addData("Alpha", "%.3f", colors.alpha);


            /* If this color sensor also has a distance sensor, display the measured distance.
             * Note that the reported distance is only useful at very close range, and is impacted by
             * ambient light and surface reflectivity. */
            if (CS instanceof DistanceSensor) {
                telemetry.addData("Distance (cm)", "%.3f", ((DistanceSensor) CS).getDistance(DistanceUnit.CM));
            }


            telemetry.update();


            // Change the Robot Controller's background color to match the color detected by the color sensor.
            relativeLayout.post(new Runnable() {
                public void run() {
                    relativeLayout.setBackgroundColor(Color.HSVToColor(hsvValues));
                }
            });

            recorder.run(-220, 228, -222, 222,0, 0.5, 5);//forward
            if(hsvValues[0]<=30&&hsvValues[0]>=330&&hsvValues[0]!=0){//sees right spike
                recorder.run(11200, 11194, 11197, 11190,0, 0.5, 5);//forward
            }
            sleep(10000);
            //turn
            //sees middle spike

            //if(hsvValues[0]<=210&&hsvValues[0]>=180&&hsvValues[0]!=0){}


        }
    }
}

